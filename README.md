# README #

### What is this repository for? ###

This repository is a documentation of the development of the program 'aapjes' starting in 2013 in the Laland Lab at St Andrews University. Originally this development took place in a different repository mixed with other programs. The commits of the different model versions were then copied to this repository to preserve the documentation of development, and to create an independent repository.

### GNU license ###

Program name: aapjes
Copyright 2016 Daniel J. van der Post

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You will find a copy of the GNU General Public License in this repository (GNU_lisence_v3.txt). If not, see http://www.gnu.org/licenses/.

### MODEL DESCRIPTION ###
Please see: van der Post et al. (2016) Skill learning and the evolution of social learning mechanisms. BMC Evolutionary Biology

### How do I get set up? ###

* Requirements: this depends on the size of the environment but using the parameter files below would require 2.2-2.3 GB of RAM.
* Compiling in Ubuntu: make
* Running program: ./aapjes[_v2015.15] parameter.cfg randomseedENV randomseedIND > output.file
* Parameter.cfg: this is a file that specifies the parameter values
* randomseedENV: this is a value that seeds the random number generator used for environmental features
* randomseedIND: this is value that seeds the random number generator used for environmental features 

### Contribution guidelines ###

I am happy for others to develop and use this code as they wish. Please contact me if you are interested.

### Who do I talk to? ###

Repo owner or admin

# Contents #

### Code and Versions ###

* Source code and makefiles: in main directory
* Versions: sub-directory with all executable versions of model
* geom: various libraries for vector calculations etc
* rstartree: libraries for storing memory in r-star-trees (for compatibility with older models, not used here)

### Shell scripts ###

* run10_script.sh: uses tsp to queue and run 10 simulations with different random seeds using particular version of model and parameter file.
* ancestor_trace_v2014.40.sh: runs ancestor_trace_v2014.40.py on raw data of 10 simulations generating files ending with _ANC.dat
* runN_script_qualAvg.sh: runs diet_weighted_avg_quality.py on raw data of 10 simulations generating files ending with _DD.dat

### Python scripts ###

* ancestor_trace_v2014.40.py: works on raw data file to trace-back ancestral lineages given parent-offspring info
* diet_weighted_avg_quality.py: calculates various repertoire statistics

### R scripts ###

These R scripts show which kind data were used for different figures (the 'simulations' section below shows the corresponding kinds of simulation):

* Fig 1C: resource_properties.R
* Fig 1D: resourceQ_distr.R

* Fig 2A: evo_overview_averages_B_H.R
* Fig 2B: evo_overview_averages_B_EC.R
* Fig 2C: evo_overview_averages_B_H_DIFF.R
* Fig 2D: evo_overview_averages_B_EC_DIFF.R

* Fig 3A: evo_fitness_boxplots_EXPL.R
* Fig 3B: info_par_prediction.R
* Fig 3C: expl_tradeoff_test_EXPL_vs_TOTALENERGY_WG_BG.R

* Fig 4A: expl_tradeoff_test_expl_vs_dd.R (YVAR=11)
* Fig 4B: expl_tradeoff_test_expl_vs_dd.R (YVAR=12)
* Fig 4C: VL_VN_AN_effectSE_lines.R (YVAR=10)
* Fig 4D: VL_VN_AN_effectSE_lines.R (YVAR=11)

* Fig S1A-D: evo_overview_averages_B_H_ANC.R
* Fig S2A-D: evo_overview_averages_B_EC_ANC.R
* Fig S3: GRNC_vs_SOL_FULLKNOW.R
* Fig S4A-C: evo_overview_averages_B_H_ANC_se.R (A), evo_overview_averages_B_H_ANC_ol.R (B,C)
* Fig S4D-F: evo_overview_averages_B_EC_ANC_se.R (D), evo_overview_averages_B_EC_ANC_ol.R (E,F)
* Fig S5: evo_overview_averages_B_DIFFENV.R (A), evo_fitness_boxplots_EXPL_DIFFENV.R (B)
* Fig S6: evo_overview_averages_B_EC1_2.R

### Parameter files ###

* Evolutionary simulations
* Parsweep simulations
* Switch simulations
* Full knowledge simulations

# Simulations #

We do not include the any data files, but we include the parameter files used to run the simulations and which model version was used. Most files (.par) can be used directly as input for model, while other (.txt) need to be converted to appropriate format (i.e. .par format). The parameters files are in directories of the corresponding simulation type:

### Fig 2 ###

* Fig 2A,C: Evolutionary simulations: H0.1,H1,H5,H10 EC5 SOL, Gr_LE, Gr_SE, Gr_OL (Parfiles: EVO_SOL/GR_LE/SE/OL_MIXED_H0.1,1,5,10_EC5_v2015.6/8.par)
* Fig 2B,D: Evolutionary simulations: H1 EC0,20,250 SOL, Gr_LE, Gr_SE, Gr_OL (Parfiles: EVO_SOL/GR_LE/SE/OL_MIXED_H1_EC0,5,20,250_v2015.6/8.par)

### Fig 3 ###

* Fig 3A: Evolutionary simulations: H1 EC5 SOL, Gr_LE, Gr_SE, Gr_OL (Parfiles: EVO_SOL/GR_LE/SE/OL_MIXED_H1_EC5_v2015.10.par)
* Fig 3C: Parsweep simulations: H1 EC5 Gr_SE (Parfiles: PS_GR_SE_MIXED_H1_EC5_EXP0_0.2/0.0001_WG/BG_v2015.10.par)

### Fig 4 ###

* Fig 4A,B: Parsweep simulations: H1 EC5 Gr_LE, Gr_SE, Gr_OL (Parfiles: PS_GR_LE/SE/OL_MIXED_H1_EC5_EXP0_0.2/0.0001_WG/BG_v2015.10.par)
* Fig 4C,D: Switch simulations: H1 EC5 Gr_LE and Gr_LE switched to Gr_SE (Parfiles: SW_GR_LE_LE/SE_MIXED_H1_EC5_EXP0.0001_0.2_BG_v2015.11.par)

### Extra simulations for appendices ###

* Fig S3 (Appendix 5): Full-knowledge sims: Effect grouping on foraging efficiency (Parfiles: FK_SOL/GR_LE_MIXED_H1_EC0_v2015.15.par)
* Fig S5 A,B (Appendix 7): Evolutionary simulations: H1 EC5, Random / Mixed Patches / Pure Patches SOL, Gr_LE, Gr_SE, Gr_OL (Parfiles: EVO_SOL/GR_LE/SE/OL_MIXED/RANDOM/PATCH_H1_EC5_v2015.6/8.par)
* Fig 6 (Appendix 8): Evolutionary simulations: H1 EC1_5, EC1_250, EC2_250, SOL, Gr_LE, Gr_SE, Gr_OL (Parfiles: EVO_SOL/GR_LE/SE/OL_MIXED_H1_EC2_250_v2015.8.par)
