// This file declares classes of individuals.

#ifndef _AAPJES_H_GUARD_
#define _AAPJES_H_GUARD_

#include <vector>
#include <list>

#include "geom.h"
#include "random.h"
#include "environment.h"
#include "resource.h"

// forward declarations
class Config;
class Environment;

class Aapje
{
  // public:
  private:
  // FIXED PSEUDO-GENES ///////////////////////////////////////////////////////////////
  enum Action {MOVEFORWARD, SEARCH, MOVETOFOOD, MOVETOGROUP, EAT, NOTHING, OBSERVE};
  int MAX_STOMACH;
  int DIG_INT;                    //digestion interval
  float stimulus_duration;
  int t_n; //nothing time
  int t_e; //eat_time
  float p_m;  //NOT USED
  int t_f; //search time
  float half_a_f; 
  float s_m; //move speed
  float d_m; //move step distance
  float s_mg;
  float d_mg; //move to group distance
  float d_mg_sq;

  // EVOLVABLE PSEUDO-GENES ////////////////////////////////////////////////////////////
  //evolvable pseudo-genes USED
  float init_pref_exp;
  float min_pref_exp;
  float pref_exp_update;
  float foodchoice_selectivity;
  float d_mf; //move fw distance
  
  //individual learning
  float initial_trial_rate;    //rate of sampling unknown resources
  float explore_rate;          //rate of sampling non-selected resources
  float explore_stimulus;

  //social learning
  float perm_social_stimulus; //added as fixed constant to pref during prob calculation
  float skill_copy_rate;   //rate at which individuals will observe
  int t_o; //observation time
  float l_M, l_f, l_a, l_t, l_p, l_r, l_c; //biases for choosing demonstrator

  //evolvable pseudo-genes NOT-USED
  float pref_exp_H_N, pref_exp_H, pref_exp_K, pref_exp_N;
  float lambda_stomach, stomach_content_estimate;
  float pe_u_n;
  float pe_u_h;
  float U_TS, U_TS_N, U_TS_INIT, U_TS_N_INIT;
  float U_PS, U_PS_N, U_PS_INIT, U_PS_N_INIT;
  float assoc_res_soc_learn_rate;
  std::vector<float> temp_social_stimulus_n;
  std::vector<int> temp_social_stimulus_nID;
  std::vector<float> perm_social_stimulus_n; //predict rewards
  std::vector<int> perm_social_stimulus_nID; //id of individual
  int copy_strategy; //NOT USED
  float S_m, S_f, S_a, S_t, S_p; //NOT USED

  // VARIABLES /////////////////////////////////////////////////////////////////////
  //variables
  static int instances;
  int parent_id;
  float energy_year, lifetime_energy;
  Environment *environment;
  float s_k;               //weighting of skill copy to define max prop of diff
                           //should be environmental variable
  //foraging and selectivity variables
  std::vector<int> stomach;
  std::vector<int> stomach_times;
  short int stomach_tot;
  float trial_rate; //set by initial_trial rate
  std::vector<Resource*> visible_resources; //used during food search algorithm
  int free_moves; //keep track of how many moves till edge of field (for speed)
  int mf_reps;    //how many steps left to take moving forard
  int mg_reps;    //how many steps left to take to move to group

  //memory ***************************************************************************
  int resource_stimulated;
  int time_since_stimulation;

  //flags for code - type of memory (but for code optimization)
  bool moved;
  bool safe;
  bool crowded;
  float predicted_reward;
  bool asoc, psoc, tsoc, tsoc_n, psoc_n;    //flags for which prediction was used
  float personal_info_prediction; 
  int perm_demon, temp_demon;  //NOT USED??
  std::vector<float> sqdist_neighs; //in order of groupmember: used to store distance for speed

  //neighbors (mainly to avoid calculating sets of neighbors repeatedly
  std::list<Aapje*> maxview_n;             //NOT USED??
  std::list<Aapje*>::iterator aapx_pos;    //NOT USED??

  //DATA COUNTERS ///////////////////////////////////////////////////////////////////
  //counters: number of times each actions occurs
  int mf; //move forward
  int fs; //food search
  int mg; //move group
  int mtf; //move to food
  int n;  //nothing
  int e;  //eat
  int o;  //observations
  int choice_asoc;
  int choice_tsoc, choice_tsoc_n;
  int choice_psoc, choice_psoc_n;

  std::vector<float> last_chosen_dem;  //0=res, 1=rel, 2=mom, 3=age, 4=freq, 5=exp, 6=pay 7=id [8]
  std::vector<float> chosen_dem;       //0=num_obs, 1=num_dem, 2=num_mom, 3=totfreq, 4=totrel, 5=totage, 6=totexp, 7=totpay [8]
  std::vector<float> tot_dem;          //0=num_obs, 1=num_dem, 2=num_mom, 3=num_res, 4=totrel, 5=totage, 6=totexp, 7=totpay, 8=freq [9]
  std::vector<float> tot_copy_se;      //0=num, 1=num_mom, 2=rel, 3=age, 4=freq, 5=exp, 6=pay [7]
  bool copy_se_flag;
  std::vector<float> tot_copy_sk;      //0=num, 1=num_mom, 2=rel, 3=age, 4=freq, 5=exp, 6=pay 7=time 8=skill increase [9]
  
  float prob_asoc;  //sum of prob_asoc
  float effect_soc; //min(socstim, 1 - prob_asoc);
  int asoc_eat;
  int soc_eat;      //num of eat events where social stimulus not redundant
  int soc_eat_and_learn; //p+C > p and pref[r]<quality[r]
  int soc_stim;     //num of eat events where social stimulus is present
  int num_stim;     //num of social stimuli
  float pref_tot_asoc;   //sum of prefs
  float fam_tot_asoc;    //sum of familiarity
  float sel_asoc;        //sum of prefexp-pref >0 = selective; <0 not selective
  float res_qual_asoc;   //sum res quality

  float pref_tot_soc;   //sum of prefs
  float fam_tot_soc;    //sum of familiarity
  float dem_age;    //sum demon age
  float dem_fam;    //sum demon familiarity
  float sel_soc;        //sum of prefexp-pref >0 = selective; <0 not selective
  float res_qual_soc;   //sum res quality
  
  // FUNCTIONS ///////////////////////////////////////////////////////////////////
  
  //initialization
  void SetRandomGroupPosition();//int + fl
  void SetRandomPosition();//int + fl
  void SetRandomHeading();
  void ReadVariables(Config c);
  void SortInXposList();//int

  //discrete updating
  void DigestAndLearn();
  void DigestContinuously();

  void SkillLearning();
  void UpdateSkill();
  float ExperienceBasedProcessingReward(int type);
  void TrialLearning();//float reward);

  void SocialLearning();
  void ChooseDemonstrator_Biases();
  void ChooseDemonstrator_NoBiases();
  void ChooseDemonstrator_Solitary();
  void ChooseDemonstrator_Oldest();
  void PermanentSocialLearning_Observations();
  void PermanentSocialLearning_RewardAssociation(float reward);
  void TemporarySocialLearning_RewardAssociation(float reward);
  void TemporarySocialStimulus();
  void PermanentSocialStimulus();
  void SocialSkillLearning(int time);

  void PreferenceExpectationUpdate();
  void AdjustSelectivityBasedOnStomachContent();
  void DataOutput();
  void DataOutputBehaviourChoice();
  void StoreBehaviourChoices(int r, bool sociallystimulated, Aapje* demon);

  //social learning
  Aapje* CopyRandom();
  Aapje* CopyOldest();
  Aapje* CopyMajority();
  Aapje* CopyIfOlder();

  void ZeroAap();
  void MutateGenome();
  void RandomizeGenome();
  float MutateGene(float gene, float step, float mutrate);
  float MutateGene(float gene, float min, float max, float step, float mutrate);

  //behaviour
  void Nothing();
  void Move(Point2d new_pos, Action action);
  Vec2d RandomVector(float angle1, float angle2);
  void MoveToFood(); //int fl
  void MoveForward();//int fl
  void Eat();
  void Search(); //int 

  void MoveOrSearch();
  void ObserveNeighbour();

  //grouping
  bool WillGroup();
  void MoveToGroup(); 
  void MoveToLeader();
  void MoveToGroup_XPOS(); //int
  void MoveToGroup_Centroid2(); //int fl    
  void MoveToGroup_RandQuad(); //int?
  int FindQuadrant(Vec2d ran_vec, Vec2d ind_vec); //int?
  void AlignToGroup();
  void AlignToLeader();
  void AlignToGroup_XPOS(); //int
  void AlignToGroup_Centroid();//int fl 

  //condition checks
  bool IsSafe();  
  bool IsSafe_XPOS(); //int
  bool IsSafe_Centroid2(); //int fl
  bool IsSafe_FollowLeader();
  void UpdateMaxviewSafeSpaceCrowding(Aapje *neigh, float xdist, int *maxv_count, int *safesp_count); //int

  //r-star
  Resource* RandomResourceInSector(Sector s);
  Resource* ClosestResourceInSector(Sector s);   
  Resource* PreferredResourceInSector();
  int SelectVisibleResources(std::vector<Resource*> *resources, std::vector<Resource*> *visible);

  float PredictedReward(int type);
  float FoodChoiceProbability(int type);
  float FoodChoiceProbability2(int type);
  float EffectSocialStimulus(float food_choice_prob);

 public:   
  Aapje(Config c, Environment *environment); //constructor1: fr
  Aapje(Environment *environment, Aapje* mother); //constructor2: from parent
  ~Aapje(); //destructor

  //variables
  bool alive;
  Action action;
  int id;    
  Aapje* follow_target;                     //for follow mom grouping
  int age;
  float energy;
  int offspring;
  float birth_energy_threshold;          //energy level at which individual can give birth
  float birth_energy_investment;         //energy given to offspring
  Resource *food_target;
  Aapje *demon, *last_demon;
  
  float U;                     //learning rate
  float temp_social_stimulus; //added as fixed constant to foodchoiceprob
  float pref_exp;
  float metabolic_costs;          //energy used per minute

  int safespace_sq;
  float safenumber;
  float prob_check_safe;

  float d_r; //reach
  float d_r_sq;
  float a_f; //serach angle
  float d_f; //search distance
  Vec2d dir;
  float d_fsq; //search dist sq to avoid recalculating

  long t_a; //time taken to complete action (after action performed)
            //needs to be reach YEAR*TIME_SCALAR*SIMULATION_TIME
  std::list<Point2d> *traj;
  Point2d pos;
  bool grouping;
  int group_id;
  std::vector<float> prefs;
  std::vector<float> reliability;
  std::vector<bool> novel;
  std::vector<int> assoc_res_soc; //association strength between res and general soc stimul.
  int assoc_res_soc_total;
  std::list<Aapje*> copyspace_n;

  std::vector< std::vector <int > > perm_social_obs_n;        //number of observations
                                             //for each neigh for each resource?
  std::vector<int> perm_social_obs_n_tot;    //for each neigh
  std::vector<int> diet;
  std::vector<int> total_processing_events;
  std::vector<float> total_processing_time;
  std::vector<bool> temp_aversion;

  //functions
  void Todo(); //deciding what to do
  void DiscreteUpdate();
  bool WillGiveBirth();
  bool WillDie();
  bool Starves();
  void Reset(); //for transmission chain - sets to zero all learning + stomach
  void InsertInXposList(); //int
  std::list<Aapje*>::iterator RemoveFromXposList(); //int
  void DataOutputDeath();
  void CopyAap(Aapje* aap);
  void GenomeRange(int root, int gene1_int, int gene2_int, float min_gene, float max_gene);

  void MakeNaiveAboutResource(int r);
};

#endif // _AAPJES_H_GUARD_
