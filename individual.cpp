#include "individual.h"
#include "branchpar.hpp"

// initialize the static members of the Individual class
int Individual::instances = 0;

Individual::Individual(float x, float y) {
  id = instances++;
  weight = INDIVIDUAL_WEIGHT;
  this->x = x;
  this->y = y;
}

int Individual::r_id() {
  return id;
}

float Individual::r_x() {
  return x;
}
 
float Individual::r_y() {
  return y;
}

float Individual::r_weight() {
  return weight;
}
