#ifndef _RESOURCE_H_GUARD_
#define _RESOURCE_H_GUARD_

#include "geom.h"
#include "rstartree/rectangle.h"
#include <vector>

// forward declarations

class ResourceType {
 public:
  long appear_time;
  long residence_time;
  float quality;
  float h_pow_n;
  int n;

  ResourceType(long apptime, long restime, float q); //constructor leanrtype == 0
  ResourceType(long apptime, long restime, float q, float h_pow_n, int n); //constructor learntype==1
};

class Resource {
 public:
  Point2d position;
  long appear_time; //TODO can be moved to resource type?
  unsigned short type;

  Resource(Point2d pos, long app_time, int t);
  Rectangle mbr();
  void GetsEaten(long time, long YEAR, long restime);
  bool IsAvailable(long time, long YEAR, long restime);
  void UpdateAppearTime(long time, long YEAR, long restime);
};

#endif // _RESOURCE_H_GUARD_
