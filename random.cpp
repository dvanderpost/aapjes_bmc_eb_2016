// In this file we initialize all random number generators (RNGs) declared in random.h

#include "random.h"

boost::mt19937 env_rng;		// basic RNG for environment
boost::mt19937 ind_rng;		// basic RNG for individuals

// we use a standard normal distribution for our RNGS
// if you want a normal distribution with different mean
// and different standard deviation, do:
// mean + (normal * std_dev)
boost::normal_distribution<> nd(0.0, 1.0);

boost::uniform_real<> ud(0.0, 1.0);

//#include <boost/random.hpp>
//#include <boost/random/gamma_distribution.hpp>
//double rgamma( double mean, double variance, boost::mt19937& rng ) {
//  const double shape = ( mean*mean )/variance;
//  double scale = variance/mean;
//  boost::gamma_distribution<> gd( shape );
//  boost::variate_generator<boost::mt19937&,boost::gamma_distribution<> > var_gamma( rng, gd );
//  return scale*var_gamma();
//}

boost::gamma_distribution<> gd(2.0); //shape parameter = (mean*mean)/variance

// initialize RNGs for environment construction
var_nor env_nor(env_rng, nd);
var_uni env_uni(env_rng, ud);

// initialize RNGs for individual behavior
var_nor ind_nor(ind_rng, nd);
var_uni ind_uni(ind_rng, ud);
var_gamma ind_gamma(ind_rng, gd);
