#include "patch.h"
#include "rstartree/rectangle.h"

//CONSTRUCTOR
Patch::Patch(int id, Point2d position, float radius)
{
  this->id = id;
  this->position = position;
  this->radius = radius;
}

//MAKES RECTANLGE FOR RSTAR_TREE
Rectangle Patch::mbr()
{
  Rectangle r;
  r.min.x = position.x-radius;
  r.min.y = position.y-radius;
  r.max.x = position.x+radius;
  r.max.y = position.y+radius;

  return r;
}
