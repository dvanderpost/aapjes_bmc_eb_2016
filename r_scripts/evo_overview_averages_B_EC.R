#use in R with source("R_scripts/evo_overview_averages_B_EC.R");

FIG=0; 
SAVEFILE_ROOT = 'data/figures/evo_overview_H1_EC0_EC5_EC250_SOL_GR_B_';
SAVEFILE_VAR='energy'; 

SAVEFILE = paste(SAVEFILE_ROOT, SAVEFILE_VAR,'.pdf', sep="");
if(FIG==1) pdf(SAVEFILE);

par(mfrow=c(1,1));
par(mar=c(5,5,5,2))

DATA=0; 
MAXSIM=10; 

LABELS=c('0', '5','20', '250');
XLAB='Rate of environmental change (EC)';
YLAB='Average energy'; 
MINY=0; 
MAXY=60000; 
MINX=0.75; 
MAXX=length(LABELS)+0.25; 
LEGEND_LOCATION='topright';
LEGEND_NAMES=c( 'SOL', expression('G'[LE]),expression('G'[SE]), expression('G'[OL]) ); 
LEGEND_LTY=c(2,1,3,4);
LEGEND_PCH=c(19,19,19,19);

MULTI_COLS=rbind(
c('grey','grey','grey','grey','grey'),
c('black','black','black','black','black'),
c('blue','blue','blue','blue','blue'),  
c('orange','orange','orange','orange','orange'),
c('red','red','red','red','red')
)

#ENVIRONMENTAL CHANGE #################################################################################################
#EC0 H1
NAME1='data/sol_evo/EVO_SOL_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_EC0_v2015.8'
NAME2='data/gr_evo/EVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS0_SSL0_EC0_v2015.8'
NAME3='data/gr_evo/EVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS1_SSL0_EC0_v2015.8'
NAME4='data/gr_evo/EVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS0_SSL1_EC0_v2015.8'
NAME5='data/gr_evo/EVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS1_SSL1_EC0_v2015.8'

#EC5 H1 Mixed patches ##################################################################################################
NAME6='data/sol_evo/EVO_SOL_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_EC1_5yr_v2015.6';
NAME7='data/gr_evo/EVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS0_SSL0_EC1_5yr_v2015.6';
NAME8='data/gr_evo/EVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS1_SSL0_NB_EC1_5yr_v2015.8';
NAME9='data/gr_evo/EVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS0_SSL1_NB_EC1_5yr_v2015.8';
NAME10='data/gr_evo/EVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS1_SSL1_NB_EC1_5yr_v2015.8';

#EC1 20yr 
NAME11='data/sol_evo/EVO_SOL_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_EC1_20yr_v2015.8'
NAME12='data/gr_evo/EVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS0_SSL0_EC1_20yr_v2015.8'
NAME13='data/gr_evo/EVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS1_SSL0_EC1_20yr_v2015.8'
NAME14='data/gr_evo/EVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS0_SSL1_EC1_20yr_v2015.8'
NAME15='data/gr_evo/EVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS1_SSL1_EC1_20yr_v2015.8'

#EC250 H1
NAME16='data/sol_evo/EVO_SOL_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS0_SSL0_EC1_250yr_v2015.8' 
NAME17='data/gr_evo/EVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS0_SSL0_EC1_250yr_v2015.8'
NAME18='data/gr_evo/EVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS1_SSL0_EC1_250yr_v2015.8'
NAME19='data/gr_evo/EVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS0_SSL1_EC1_250yr_v2015.8'
NAME20='data/gr_evo/EVO_GR0_GT40_R250_Q0.1_QN0.1_N1_4_H1_1_LN0.005_P24500_PR5_3_1200_A6261_IPS0_ERS0_TSS1_SSL1_EC1_250yr_v2015.8'

NAMES=rbind(c(NAME1, NAME6, NAME11, NAME16),c(NAME2, NAME7, NAME12, NAME17), c(NAME3, NAME8, NAME13, NAME18),c(NAME4, NAME9, NAME14, NAME19));
print(NAMES);
DAT='.dat'

plot(-100,-100,xlim=c(MINX,MAXX), ylim=c(MINY, MAXY), xlab=XLAB, ylab='Average energy', xaxt='n', cex.lab=1.5, cex.axis=1.5); lines(c(-1,10),c(5000,5000));
axis(1, at=seq(MINX+0.25,MAXX-0.25,1),labels=LABELS, col.axis="black", las=1, cex.lab=1.5, cex.axis=1.15)		

if(DATA==1){
t1=0; tt1=0;
t2=0; tt2=0;
t3=0; tt3=0;
t4=0; tt4=0;

it=0; #counter
for(r in 1:nrow(NAMES)){
for(c in 1:ncol(NAMES))
{

if(c==1) {it=0; means=0;}
it=it+1;

if(NAMES[r,c]!='NULL')
{

if(c==1) x_means = 1 + ((r-3)*0.1)
else x_means = rbind(x_means, (c + ((r-3)*0.1)));

t=0;

for(i in 1:MAXSIM)
{
filename = paste(NAMES[r,c],'_is',i,'_rs',i,DAT, sep='');

print(c(it, filename));
str=paste("grep '^A' ",filename," |  cut -d' ' -f2,12", sep="");
d=read.table(pipe(str),header=FALSE, sep=" ");

s = subset(d, d[,1]>=950 & d[,1]<=1000);
if(i==1) {t = mean(s[,2]);};
if(i>1) {t = rbind(t,mean(s[,2]));};
print(c(i, mean(s[,2])));

if(r==1 & c==1){if(i==1){t1=s;}; if(i>1){t1=rbind(t1,s);}}
if(r==1 & c==2){if(i==1){t2=s;}; if(i>1){t2=rbind(t2,s);}}
if(r==2 & c==1){if(i==1){t3=s;}; if(i>1){t3=rbind(t3,s);}}
if(r==2 & c==2){if(i==1){t4=s;}; if(i>1){t4=rbind(t4,s);}}
}#for i in maxsim

points(x_means[it],mean(t[,1]), col=MULTI_COLS[r,c], pch=19, cex=1.5);
	
if(r==1 & c==1) tt1 = t;
if(r==1 & c==2) tt2 = t;
if(r==2 & c==1) tt3 = t;
if(r==2 & c==2) tt4 = t;

#ST DEV
lines(c(x_means[it],x_means[it]), c(mean(t[,1])-sd(t[,1]), mean(t[,1])+sd(t[,1])), col=MULTI_COLS[r,c],lwd=2)
if(c==1) {means = mean(t[,1]);}
else {means = rbind(means, mean(t[,1]));}
print(c(it, mean(t[,1]),sd(t[,1])));
}#if not NULL
}#for all names

lines(x_means, means, col=MULTI_COLS[r,c], lwd=2, lty=LEGEND_LTY[r])
print(means)
print(x_means)

if(r==1) sol = means;
if(r==2) grnc = means;
if(r==3) grse = means;
if(r==4) grol = means;
if(r==5) grseol = means;

}#for nrows names
}# end DATA==1

for(r in 1:nrow(NAMES)){
x_means=seq(1,4,1);
}#end in row names

d=0;
s=0;
t=0;

LEGEND_COLS = MULTI_COLS[,1]
LEGEND_COLS[LEGEND_COLS=='white']='black'
legend(LEGEND_LOCATION, legend=LEGEND_NAMES, col=LEGEND_COLS, lty=LEGEND_LTY, lwd=rep(2,nrow(NAMES)), bg='white');

if(FIG==1) dev.off();

