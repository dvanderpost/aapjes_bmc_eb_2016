#ifndef _PATCH_H_GUARD
#define _PATCH_H_GUARD

#include <vector>
#include "geom.h"

// forward declarations
class Rectangle;

class Patch
{
public:
  //VARIABLES
  int id;
  Point2d position; 
  float radius;  
  std::vector<int> resourcetypes;

  //FUNCTIONS
  Patch(int id, Point2d position, float radius); //CONSTRUCTOR
  Rectangle mbr(); //GENERATE RSTAR_TREE RECTANGLE
};

#endif // _PATCH_H_GUARD
