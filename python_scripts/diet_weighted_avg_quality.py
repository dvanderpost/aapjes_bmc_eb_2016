#!/usr/bin/python
#WARNING: need to set POPSIZE and STARTTIME here! 

from random import randrange
from string import split
import sys
import time
from os import listdir, getcwd
from numpy.linalg import norm
from numpy import dot
from numpy import log
from subprocess import check_output

#TODO:
# script to calculate per diet D or pref P output -> the relavant quality measures
# get lines with "^Q" -> quality: time, q1:q250 (roughly 800 lines)

filename = sys.argv[1] #needs to be file of prefs of diet amounts per time unit

PATTERN='^Q'; #only get QN lines because is sufficient and avoid problem of different lengths
output = check_output(["grep", PATTERN, filename]);
q = output.split('\n');

PATTERN='^P ';
output = check_output(["grep", PATTERN, filename]);
p = output.split('\n');

PATTERN='^D ';
output = check_output(["grep", PATTERN, filename]);
d = output.split('\n');

PATTERN='^N ';
output = check_output(["grep", PATTERN, filename]);
n = output.split('\n');
nl = n[0].split();
nf = [float(l) for l in nl[2:len(nl)]]
h=1; #scaled from 720 to 1
hn = [h**b for b in nf]

T_SWITCH=1;
if(T_SWITCH==0):
	PATTERN='^A ';
	output = check_output(["grep", PATTERN, filename]);
	aa = output.split('\n');

if(T_SWITCH==1):
	PATTERN='^T ';
	output = check_output(["grep", PATTERN, filename]);
	t = output.split('\n');

q_iter=0;
ql = q[q_iter].split();
if(ql[0]=='Q'):
	ql.insert(1,0);
	#print ql

for i in range(0,len(d)-1): #avoid last line which is empty

	dl = d[i].split(); #get diet data line

	#GET RELEVANT QUALITY LINE
	while q_iter<(len(q)-1) and float(ql[1]) < float(dl[1]): #update until one time step ahead
		q_iter=q_iter+1
		ql = q[q_iter].split();     
	qll = q[q_iter-1].split();           #get relevant time point before
	if(qll[0]=='Q'):
		qll.insert(1,0);
	#print qll
	#raw_input()

	#TOTAL EATEN + FRACTIONS EATEN OF EACH FOOD
	df = [float(l) for l in dl[5:len(dl)]] #convert amounts eaten to floats
	sum_df = sum(df)
	if sum_df>0: #check for divide by zero
		dp = [l/sum_df for l in df]
	else:
		dp = [0 for l in df]

	#DIET WEIGHTED AVERAGE QUALITY
	qf = [float(l) for l in qll[2:len(qll)]] #convert amounts eaten to floats
	sum_qf = sum([a*b for a,b in zip(dp,qf)])

	#DIET WEIGHTED SKILL LEVEL [between 0 and 1]
	if(T_SWITCH==0):
		al = aa[i].split()

	if(T_SWITCH==1):
		tl = t[i].split(); #get data line
		tf = [float(l) for l in tl[5:len(tl)]]
		sum_tf = sum([a*b for a,b in zip(df,tf)])	
		tf = [a/720 for a in tf]
		tn = [a**b for a,b in zip(tf,nf)]
		sk = [a/(a+b) for a,b in zip(tn,hn)]
		sum_sk = sum([a*b for a,b in zip(dp,sk)])

	#DIET WEIGHTED AVERAGE REWARD
	pl = p[i].split(); #get data line
	pf = [float(l) for l in pl[5:len(pl)]] #TODO: check if starts at 5!
	avg_pf = sum([a*b for a,b in zip(dp,pf)])

	#TOTAL DIET ENERGY
	dE = sum([a*b for a,b in zip(df,pf)])	
	
	if(float(dl[3])<0.5):
		sum_df = (float(sum_df)*0.5)/float(dl[3]);

	#	1=T	2=I	3=A	4=Qtime	5=TotF	6=avgR	7=dietE	8=avgT	9=avgQ	10=avgSK
	if(T_SWITCH==1):
		print dl[1], dl[2], dl[3], qll[1], sum_df, avg_pf, dE, sum_tf, sum_qf, sum_sk
	else: #for EVO
	#	1=T	2=I	3=A	4=Qtime	5=TotF	6=avgR	7=dietE	8=avgT	9=avgQ	10=avgSK 11=explrate 12=group size
		print dl[1], dl[2], dl[3], qll[1], sum_df, avg_pf, dE, -1, sum_qf, -1, float(al[43]), float(al[27])
