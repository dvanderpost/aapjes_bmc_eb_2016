#!/usr/bin/python

from string import split
import sys
#from numpy.linalg import norm
#from numpy import dot
#import math                     #math.log( x )

filename = sys.argv[1] 
infile = file( filename )

#GENOME DATA (numbers indicate column in raw data)
#1 time;  
#2 id #3 mom
#7 initial trial rate;  #8 pref-exp update;  #9 selectivity;
#10 num offspring;
#11 initprefexp; 
#14 d_mf;   #15 U learnrate;    #19 min pref exp;
#20 U_TS;   #21 P_TS;  #22 U_TS_N;   #23 U_PS_N
#12 temp soc stim
#13 perm soc stim
#24 U_TS_INIT #25 U_PS_INIT #26 U_TS_N_INIT #27 U_PS_N_INIT
#28 explore_rate #29 explore_stimulus
#30 pref_exp_N #31 pref_exp_K #32 pref_exp_H #33 lambda_stomach
#34-40 lR, lC, lM, lF, lA, lT, lP
#41-43 pSk, sk, tO

#1=time 2=id 3=mom 4=pT 5=lE 6=Ss 7=OFF 8=E_I 9=dM 10=lI 11=E_M 12-15=U_* 16=pS 17=aS 
#18-21=U_INIT* 22=pE 23=lR 24-27=prefexp*
#28-34 lR, lC, lM, lF, lA, lT, lP
#35-37 pSk, sk, tO

count=0
for i in infile:
	l = i.split()
	mom = int(l[6])			
	print float(l[1]), int(l[2]), int(l[6]), float(l[7]), float(l[8]), float(l[9]), int(l[10]), float(l[11]), float(l[14]), float(l[15]), float(l[19]), float(l[20]), float(l[21]), float(l[22]), float(l[23]), float(l[12]), float(l[13]), float(l[24]), float(l[25]), float(l[26]), float(l[27]), float(l[28]), float(l[29]), float(l[30]), float(l[31]), float(l[32]), float(l[33]), float(l[34]), float(l[35]), float(l[36]), float(l[37]), float(l[38]), float(l[39]), float(l[40]), float(l[41]), float(l[42]), float(l[43])
	infile2 = file( filename )
	for j in infile2:
		l2 = j.split()
		focal2 = int(l2[2])
		if(focal2==mom):
			print float(l2[1]), int(l2[2]), int(l2[6]), float(l2[7]), float(l2[8]), float(l2[9]), int(l2[10]), float(l2[11]), float(l2[14]), float(l2[15]), float(l2[19]), float(l2[20]), float(l2[21]), float(l2[22]), float(l2[23]), float(l2[12]), float(l2[13]), float(l2[24]), float(l2[25]), float(l2[26]), float(l2[27]), float(l2[28]), float(l2[29]), float(l2[30]), float(l2[31]), float(l2[32]), float(l2[33]), float(l2[34]), float(l2[35]), float(l2[36]), float(l2[37]), float(l2[38]), float(l2[39]), float(l2[40]), float(l2[41]), float(l2[42]), float(l2[43])
			#raw_input('');
			mom=int(l2[6])
	count = count + 1
	if count == 200:
		break
infile.close();

