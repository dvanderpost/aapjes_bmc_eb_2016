#ifndef _GEOM_H_GUARD_
#define _GEOM_H_GUARD_

#include "vec2d.h"
#include "point2d.h"
#include "segment.h"
#include "rectangle.h"

#endif // _GEOM_H_GUARD_
