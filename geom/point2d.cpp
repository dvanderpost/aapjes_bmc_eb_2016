#include "point2d.h"
#include "vec2d.h"

Point2d Point2d::operator +(Vec2d v) {
  Point2d r;
  r.x = x + v.x;
  r.y = y + v.y;

  return r;
}

Point2d Point2d::operator -(Vec2d v) {
  Point2d r;
  r.x = x - v.x;
  r.y = y - v.y;

  return r;
}

bool Point2d::operator==(const Point2d &p) const
{
  return x == p.x && y == p.y;
}

Vec2d Point2d::operator -(Point2d q) {
  Vec2d r;
  r.x = x - q.x;
  r.y = y - q.y;

  return r;
}
