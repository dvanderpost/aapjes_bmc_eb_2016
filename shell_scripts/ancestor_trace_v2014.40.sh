#tsch
#FUNCTION: 
# -- sorts lifehistory data in reverse according to ind ID
# -- runs python ancestor trace

#$1 = directory + file without '_isX_rsX.dat'

for i in {1..10}
	do
		grep '^L' $1'_is'$i'_rs'$i'.dat' | sort -n | sort -n -r -k 3 > $1'_is'$i'_rs'$i'_temp.dat'
		python ~/repos/soclearn_evo/python_scripts/ancestor_trace_v2014.40.py $1'_is'$i'_rs'$i'_temp.dat' > $1'_is'$i'_rs'$i'_temp_anc.dat'
		sort -n -u -k 1 $1'_is'$i'_rs'$i'_temp_anc.dat' > $1'_is'$i'_rs'$i'_ANC.dat'
		rm -f $1'_is'$i'_rs'$i'_temp.dat'
		rm -f $1'_is'$i'_rs'$i'_temp_anc.dat'
		echo $1'_is'$i'_rs'$i'_ANC.dat'
	done

